import React, {Component} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import './App.css';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Home from './components/Home/Home';
import Offer from './components/Offer/Offer';
import Portfolio from './components/Portfolio/Portfolio';
import Contact from './components/Contact/Contact';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Header />
          <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/oferta' component={Offer} />
            <Route path='/portfolio' component={Portfolio} />
            <Route path='/kontakt' component={Contact} />
            <Route component={Home} />
          </Switch>
          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
