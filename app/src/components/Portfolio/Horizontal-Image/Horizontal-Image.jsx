import React from 'react';
import './Horizontal-Image.css';


export default function HorizontalImage(props) {

  return (
    <div className='image-holder horizontal-image-holder'>
      <img src={props.data[0]} className='portfolio-image' alt={props.data[1]}/>
    </div>
  );
}