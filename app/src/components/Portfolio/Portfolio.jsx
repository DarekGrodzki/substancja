import React from 'react';
import './Portfolio.css';
import VerticalImage from './Vertical-Image/Vertical-Image';
import HorizontalImage from './Horizontal-Image/Horizontal-Image';
import nbp from '../../assets/images/portfolio/nbp.jpg';
import polsat from '../../assets/images/portfolio/polsat.jpg';
import manpower from '../../assets/images/portfolio/manpower.jpg';
import liz from '../../assets/images/portfolio/liz.jpg';
import sharp from '../../assets/images/portfolio/sharp.jpg';
import wp from '../../assets/images/portfolio/wp.jpg';
import embassy from '../../assets/images/portfolio/embassy.jpg';
import kcp from '../../assets/images/portfolio/kcp.jpg';
import dvision from '../../assets/images/portfolio/dvision.jpg';
import manpower2 from '../../assets/images/portfolio/manpower2.jpg';
import sgb from '../../assets/images/portfolio/sgb.jpg';
import travelport from '../../assets/images/portfolio/travelport.jpg';
import utw from '../../assets/images/portfolio/utw.jpg';
import cnk from '../../assets/images/portfolio/cnk.jpg';
import wp2 from '../../assets/images/portfolio/wp2.jpg';
import kcp2 from '../../assets/images/portfolio/kcp2.jpg';
import cropp from '../../assets/images/portfolio/cropp.jpg';
import inpost from '../../assets/images/portfolio/inpost.jpg';
import bosch from '../../assets/images/portfolio/bosch.jpg';
import sgb2 from '../../assets/images/portfolio/sgb2.jpg';
import kcp3 from '../../assets/images/portfolio/kcp3.jpg';
import mediatech from '../../assets/images/portfolio/mediatech.jpg';
import kowalczyk from '../../assets/images/portfolio/kowalczyk.jpg';
import substancja from '../../assets/images/portfolio/substancja.jpg';

export default function Portfolio () {

  const vertical = [
    [nbp, 'Roll-up wydrukowany dla NBP'],
    [polsat, 'Roll-up wydrukowany dla Polsat Jim-Jam'],
    [manpower, 'Roll-up wydrukowany dla Manpower'],
    [liz, 'Roll-up wydrukowany dla Liz'],
    [sharp, 'Roll-up wydrukowany dla Sharp'],
    [wp, 'wystawa wydrukowana dla Wojska Polskiego'],
    [embassy, 'Roll-up wydrukowany dla ambasady Holandii'],
    [kcp, 'Roll-up wydrukowany dla King Cross Praga'],
    [dvision, 'Roll-up wydrukowany dla D-Vision'],
    [manpower2, 'Pop-up wydrukowany dla Manpower'],
    [sgb, 'Pop-up wydrukowany dla SGB'],
    [travelport, 'Pop-up wydrukowany dla Travelport']
  ];

  const horizontal = [
    [utw, 'wystawa wydrukowana dla Uniwersytetu Trzeciego Wieku'],
    [cnk, 'wystawa wydrukowana dla Centrum Nauki Kopernik'],
    [wp2, 'wystawa wydrukowana dla Wojska Polskiego'],
    [kcp2, 'Oklejenie witryn w King Cross Praga'],
    [cropp, 'Oklejenie witryn w sklepie Cropp'],
    [inpost, 'Tablice reklamowe dla Inpost'],
    [bosch, 'Grafika ścienna dla firmy Bosch'],
    [sgb2, 'Oklejenie witryn w Banku Spółdzielczym w Halinowie'],
    [kcp3, 'Oklejenie witryn w King Cross Praga'],
    [mediatech, 'Oklejenie samochodu dla firmy Media-Tech'],
    [kowalczyk, 'Oklejenie samochodu dla firmy Kowalczyk'],
    [substancja, 'Oklejenie naszego firmowego samochodu']
  ];

  const verticalImages = vertical.map((element) => {
    return <VerticalImage data={element} key={element[0]} />
  });

  const horizontalImages = horizontal.map((element) => {
    return <HorizontalImage data={element} key={element[0]} />
  });

  return (
    <section className='portfolio'>
      {verticalImages}
      {horizontalImages}
    </section>
  );
}