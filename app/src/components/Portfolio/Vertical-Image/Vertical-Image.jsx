import React from 'react';
import './Vertical-Image.css';


export default function VerticalImage (props) {

  return (
    <div className='image-holder vertical-image-holder'>
      <img src={props.data[0]} className='portfolio-image' alt={props.data[1]} />
    </div>
  );
}