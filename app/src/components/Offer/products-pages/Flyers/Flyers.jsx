import React from 'react';
import flyers from '../../../../assets/images/products/ulotki.jpg';
import folders from '../../../../assets/images/products/foldery.jpg';
import posters from '../../../../assets/images/products/plakaty.jpg';
import postersSecond from '../../../../assets/images/products/plakaty-1.jpg';

export default function Flyers() {
  return (
    <div className="content-container">
      <div className="offer-group">
        <h2>Ulotki, foldery</h2>
        <p>
          Drukujemy je przy mniejszych nakładach w technologii cyfrowej lub przy większych offsetowo. Mamy możliwość
          wykonania bardzo wielu produktów takich jak:
          <span>- ulotki - zwykłe i składane</span>
          <span>- teczki ofertowe,</span>
          <span>- prezentacje - bindowane i szyte,</span>
          <span>- foldery i katalogi,</span>
          <span>- zaproszenia,</span>
          <span>- i wiele innych.</span>
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={flyers} alt="ulotki reklamowe"/>
          </div>
          <div className="offer-image">
            <img src={folders} alt="foldery reklamowe"/>
          </div>
        </div>
        <p>
          W związku z tym, że gama produktów druku małoformatowego jest ogromna nie publikujemy na naszej stronie całej
          oferty. Jeśli chcesz dowiedzieć się więcej lub otrzymać wycenę, zadzwoń lub napisz do nas maila, z
          przyjemnością
          odpowiemy na wszystkie Twoje pytania.
        </p>
        <p>Poniżej kilka przykładowych cen ulotek - ceny netto.</p>
        <div className="offer-table">
          <div className="subtitle">Ulotki offsetowe, dwustronne, kreda matowa 150g</div>
          <div className="product-name">Format / Ilość</div>
          <div className="price">1000</div>
          <div className="price">2000</div>
          <div className="product-name">DL</div>
          <div className="price">156</div>
          <div className="price">200</div>
          <div className="product-name">A6</div>
          <div className="price">146</div>
          <div className="price">170</div>
          <div className="product-name">A5</div>
          <div className="price">232</div>
          <div className="price">300</div>
          <div className="product-name">A4</div>
          <div className="price">382</div>
          <div className="price">472</div>
        </div>
      </div>
      <div className="offer-group">
        <h2>Plakaty</h2>
        <p>
          <span>
            - potrzebujesz wydrukować plakaty? Drukujemy już od nakładu 1 szt. Maksymalna szerokość to 150 cm, długość
          dowolna. Wydruki w pełnym kolorze z fotograficzną jakością. Drukujemy na kilku rodzajach papieru:
          </span>
          <span>- Papier satynowany(citylight) 135 g/m2,</span>
          <span>- Papier satynowany 200 g/m2,</span>
          <span>- Papier fotograficzny/błyszczaćy 260 g/m2,</span>
          <span>- Blueback 115 g/m2 lub</span>
          <span>- Papier lateksowy.</span>
        </p>
        <p>
          Jeżeli potrzebujesz dużego nakładu plakatów, chętnie wycenimy je dla Ciebie także w technologii offsetowej.
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={posters} alt="plakaty"/>
          </div>
          <div className="offer-image">
            <img src={postersSecond} alt="plakaty"/>
          </div>
        </div>
        <div className="offer-table">
          <div className="subtitle">Ulotki offsetowe, dwustronne, kreda matowa 150g</div>
          <div className="product-name">Format / Ilość</div>
          <div className="price">1000</div>
          <div className="price">2000</div>
          <div className="product-name">DL</div>
          <div className="price">156</div>
          <div className="price">200</div>
          <div className="product-name">A6</div>
          <div className="price">146</div>
          <div className="price">170</div>
          <div className="product-name">A5</div>
          <div className="price">232</div>
          <div className="price">300</div>
          <div className="product-name">A4</div>
          <div className="price">382</div>
          <div className="price">472</div>
        </div>
      </div>
    </div>
  )
}