import React from 'react';
import windowWrapping from '../../../../assets/images/products/oklejanie-witryn.jpg';
import windowWrappingSecond from '../../../../assets/images/products/oklejanie-witryn-1.jpg';
import carWrapping from '../../../../assets/images/products/oklejanie-samochodow.jpg';
import carWrappingSecond from '../../../../assets/images/products/oklejanie-samochodow-1.jpg';

export default function Wrapping() {
  return (
    <div className="content-container">
      <div className="offer-group">
        <h2>Oklejanie</h2>
        <h2>Oklejanie witryn sklepowych</h2>
        <p>
          <span>Chcesz wyróżnić swój sklep, restaurację lub inny lokal na tle konkurencji?</span>
          <span>
            Chcesz poprzez estetyczny wygląd i reklamę zachęcić potencjalnych klientów do korzystania ze swoich usług?
          </span>
          <span>Oferujemy profesjonalne oklejanie witryn sklepowych.</span>
        </p>
        <p>
          <span>Jest kilka opcji oklejania:</span>
          <span>- pełną folią zadrukowaną w dowolny sposób,</span>
          <span>
            - folią one way vision czyli zadrukowaną, dziurkowaną folią która daje efekt z jednej strony widocznej
            grafiki, a z drugiej jest widoczne to co jest na zewnątrz,
          </span>
          <span>
            - pełnymi, ozdobnymi foliami, występującymi w wielu kolorach i opcjach między innymi foliami szronionymi.
            Takie folie mogą być za pomocą plotera tnącego wycinane w różne kształty oraz napisy.
          </span>
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={windowWrapping} alt="oklejanie witryn sklepowych"/>
          </div>
          <div className="offer-image">
            <img src={windowWrappingSecond} alt="oklejanie witryn sklepowych"/>
          </div>
        </div>
        <p>
          Nasza usługa jest kompleksowa, składa się na nią zarówno projekt, druk jak i sam montaż. Staramy się zawsze
          używać jak najlepszych materiałów, aby witryny przez nas oklejone wyglądały świetnie przez długi czas. Koszt
          oklejania jest uzależniony od wielu czynników takich jak wielkość witryn, rodzaj folii, poziom trudności itp.,
          dlatego w celu jego poznania prosimy o kontakt z nami.
        </p>
      </div>
      <div className="offer-group">
        <h2>Oklejanie samochodów</h2>
        <p>
          Łącząc kreatywność przy projektowaniu grafiki z doświadczeniem zbieranym przy każdej kolejnej aplikacji,
          oferujemy naszym klientom fachową usługę znakowania samochodów firmowych. Dzięki temu środki transportu mogą
          się stać bardzo efektywną, mobilną powierzchnią reklamową usług Państwa firmy...
        </p>
        <p>
          ...a dla fanów tuningu też znajdziemy ciekawe rozwiązanie. Zapraszamy.
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={carWrapping} alt="oklejanie samochodów"/>
          </div>
          <div className="offer-image">
            <img src={carWrappingSecond} alt="oklejanie samochodów"/>
          </div>
        </div>
        <p>
          Każde oklejenie samochodu jest wyceniane indywidualnie, zapraszamy do kontaktu.
        </p>
      </div>
    </div>
  )
}