import React from 'react';
import standardOpen from '../../../../assets/images/products/roll-up-standard-open.jpg';
import standard from '../../../../assets/images/products/roll-up-standard.jpg';
import ovalOpen from '../../../../assets/images/products/roll-up-oval-open.jpg';
import oval from '../../../../assets/images/products/roll-up-oval.jpg';
import standardPlusOpen from '../../../../assets/images/products/roll-up-standard-plus-open.jpg';
import standardPlus from '../../../../assets/images/products/roll-up-standard-plus.jpg';
import smartOpen from '../../../../assets/images/products/roll-up-smart-open.jpg';
import smart from '../../../../assets/images/products/roll-up-smart.jpg';
import smartPlusOpen from '../../../../assets/images/products/roll-up-smart-plus-open.jpg';
import smartPlus from '../../../../assets/images/products/roll-up-smart-plus.jpg';
import premiumOpen from '../../../../assets/images/products/roll-up-premium-open.jpg';
import premium from '../../../../assets/images/products/roll-up-premium.jpg';
import standardDoubleOpen from '../../../../assets/images/products/roll-up-standard-double-open.jpg';
import standardDouble from '../../../../assets/images/products/roll-up-standard-double.jpg';
import smartDoubleOpen from '../../../../assets/images/products/roll-up-smart-double-open.jpg';
import smartDouble from '../../../../assets/images/products/roll-up-smart-double.jpg';

export default function Rollups() {

  return (
    <div className="content-container">
      <div className="offer-group">
        <h2>Standard</h2>
        <p>Lekka, wykonana z aluminium standardowa konstrukcja w komplecie z czarną torbą transportową. Występuje w 2
          rozmiarach 85 lub 100 cm szerokości na 200 cm wysokości.
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={standardOpen} alt="roll-up standard"/>
          </div>
          <div className="offer-image">
            <img src={standard} alt="roll-up standard"/>
          </div>
        </div>
        <div className="offer-table">
          <div className="subtitle">Ceny pln netto za 1 sztukę z wydrukiem / rodzaje podłoża do druku</div>
          <div className="product-name">Szerokość w cm</div>
          <div className="price">85</div>
          <div className="price">100</div>
          <div className="product-name">Blockout</div>
          <div className="price">129</div>
          <div className="price">139</div>
        </div>
      </div>
      <div className="offer-group">
        <h2>Oval</h2>
        <p>Kaseta wykonana z aluminium o ładnym owalnym kształcie. Występuje w 2 rozmiarach 85 lub 100 cm szerokości na
          200 cm wysokości. Zaletą tego systemu, oprócz ciekawego kształu jest torba transportowa, która ma suwak na
          całej szerokości przez co wyjmowanie i wkładanie kasety jest łatwiejsze.
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={ovalOpen} alt="roll-up oval"/>
          </div>
          <div className="offer-image">
            <img src={oval} alt="roll-up oval"/>
          </div>
        </div>
        <div className="offer-table">
          <div className="subtitle">Ceny pln netto za 1 sztukę z wydrukiem / rodzaje podłoża do druku</div>
          <div className="product-name">Szerokość w cm</div>
          <div className="price">85</div>
          <div className="price">100</div>
          <div className="product-name">Blockout</div>
          <div className="price">175</div>
          <div className="price">210</div>
        </div>
      </div>
      <div className="offer-group">
        <h2>Standard+</h2>
        <p>
          Wykonana w pełni z aluminium, sprawdzona konstrukcja w komplecie z czarną torbą transportową. Jest dużo
          bardziej stabilna i wytrzymała od wersji standard m.in. ze względu na to, że jest wykonana z grubszego
          aluminium. Roll up ten posiada dwie nóżki oraz jeden lub dwa słupki w zależności od szerokości. Występuje w 4
          rozmiarach 85, 100, 120 lub 150 cm szerokości na 200 cm wysokości. Opcjonalnie można zamówić lampkę do
          oświetlenia kasety.
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={standardPlusOpen} alt="roll-up standard+"/>
          </div>
          <div className="offer-image">
            <img src={standardPlus} alt="roll-up standard+"/>
          </div>
        </div>
        <div className="offer-table">
          <div className="subtitle">Ceny pln netto za 1 sztukę z wydrukiem / rodzaje podłoża do druku</div>
          <div className="product-name">Szerokość w cm</div>
          <div className="price">85</div>
          <div className="price">100</div>
          <div className="price">120</div>
          <div className="price">150</div>
          <div className="product-name">Blockout</div>
          <div className="price">199</div>
          <div className="price">229</div>
          <div className="price">X</div>
          <div className="price">X</div>
          <div className="product-name">Baner powlekany</div>
          <div className="price">199</div>
          <div className="price">229</div>
          <div className="price">264</div>
          <div className="price">304</div>
          <div className="product-name">Baner ferrari</div>
          <div className="price">239</div>
          <div className="price">275</div>
          <div className="price">320</div>
          <div className="price">399</div>
        </div>
      </div>
      <div className="offer-group">
        <h2>Smart</h2>
        <p>
          Ekonomiczne kasety o kształcie łezki w komplecie z czarną torbą transportową. Roll up posiada jeden lub dwa
          słupki w zależności od szerokości. Występuje w rozmiarach 85, 100 lub 120 cm szerokości na 200 cm wysokości.
          Występują także w kolorze czarnym.
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={smartOpen} alt="roll-up smart"/>
          </div>
          <div className="offer-image">
            <img src={smart} alt="roll-up smart"/>
          </div>
        </div>
        <div className="offer-table">
          <div className="subtitle">Ceny pln netto za 1 sztukę z wydrukiem / rodzaje podłoża do druku</div>
          <div className="product-name">Szerokość w cm</div>
          <div className="price">85</div>
          <div className="price">100</div>
          <div className="price">120</div>
          <div className="product-name">Blockout</div>
          <div className="price">289</div>
          <div className="price">329</div>
          <div className="price">x</div>
          <div className="product-name">Baner powlekany</div>
          <div className="price">289</div>
          <div className="price">329</div>
          <div className="price">379</div>
          <div className="product-name">Baner ferrari</div>
          <div className="price">329</div>
          <div className="price">369</div>
          <div className="price">435</div>
          <div className="product-name">Poliester</div>
          <div className="price">349</div>
          <div className="price">389</div>
          <div className="price">475</div>
        </div>
      </div>
      <div className="offer-group">
        <h2>Smart+</h2>
        <p>
          Aluminiowe kasety o kształcie łezki wykonane z najwyższej jakości materiałów w komplecie z czarną, wzmocnioną
          torbą transportową. Roll up posiada regulację pochylenia grafiki oraz jeden lub dwa słupki w zależności od
          szerokości. Występuje w 4 rozmiarach 85, 100 na 200 cm wysokości i 120 lub 150 cm szerokości na 215 cm
          wysokości. Opcjonalnie można zamówić lampkę do oświetlenia kasety.
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={smartPlusOpen} alt="roll-up smart+"/>
          </div>
          <div className="offer-image">
            <img src={smartPlus} alt="roll-up smart+"/>
          </div>
        </div>
        <div className="offer-table">
          <div className="subtitle">Ceny pln netto za 1 sztukę z wydrukiem / rodzaje podłoża do druku</div>
          <div className="product-name">Szerokość w cm</div>
          <div className="price">85</div>
          <div className="price">100</div>
          <div className="price">120</div>
          <div className="price">150</div>
          <div className="product-name">Blockout</div>
          <div className="price">317</div>
          <div className="price">365</div>
          <div className="price">X</div>
          <div className="price">X</div>
          <div className="product-name">Baner powlekany</div>
          <div className="price">317</div>
          <div className="price">365</div>
          <div className="price">399</div>
          <div className="price">460</div>
          <div className="product-name">Baner ferrari</div>
          <div className="price">362</div>
          <div className="price">405</div>
          <div className="price">455</div>
          <div className="price">545</div>
          <div className="product-name">Poliester</div>
          <div className="price">382</div>
          <div className="price">425</div>
          <div className="price">495</div>
          <div className="price">X</div>
        </div>
      </div>
      <div className="offer-group">
        <h2>Premium</h2>
        <p>
          Aluminiowe kasety wykonane z najwyższej jakości materiałów w komplecie z czarną, wzmocnioną torbą
          transportową. Roll up posiada regulację pochylenia grafiki oraz jeden lub dwa słupki w zależności od
          szerokości. Występuje w 3 rozmiarach 85, 100 lub 120 cm, a wysokość grafiki jest regulowana w zakresie od 160
          do 220 cm. Opcjonalnie można zamówić lampkę do oświetlenia kasety.
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={premiumOpen} alt="roll-up premium"/>
          </div>
          <div className="offer-image">
            <img src={premium} alt="roll-up premium"/>
          </div>
        </div>
        <div className="offer-table">
          <div className="subtitle">Ceny pln netto za 1 sztukę z wydrukiem / rodzaje podłoża do druku</div>
          <div className="product-name">Szerokość w cm</div>
          <div className="price">85</div>
          <div className="price">100</div>
          <div className="price">120</div>
          <div className="product-name">Baner ferrari</div>
          <div className="price">434</div>
          <div className="price">525</div>
          <div className="price">595</div>
          <div className="product-name">Poliester</div>
          <div className="price">454</div>
          <div className="price">545</div>
          <div className="price">635</div>
        </div>
      </div>
      <div className="offer-group">
        <h2>Standard dwustronny</h2>
        <p>
          Dwustronne, aluminiowe kasety w w komplecie z czarną torbą transportową. Roll upy te występują w dwóch
          rozmiarach 85 lub 100 cm szerokości na 200 cm wysokości. Opcjonalnie można zamówić lampkę do oświetlenia
          kasety.
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={standardDoubleOpen} alt="roll-up standard dwustronny"/>
          </div>
          <div className="offer-image">
            <img src={standardDouble} alt="roll-up standard dwustronny"/>
          </div>
        </div>
        <div className="offer-table">
          <div className="subtitle">Ceny pln netto za 1 sztukę z wydrukiem / rodzaje podłoża do druku</div>
          <div className="product-name">Szerokość w cm</div>
          <div className="price">85</div>
          <div className="price">100</div>
          <div className="product-name">Blockout</div>
          <div className="price">329</div>
          <div className="price">369</div>
          <div className="product-name">Baner powlekany</div>
          <div className="price">329</div>
          <div className="price">369</div>
          <div className="product-name">Baner ferrari</div>
          <div className="price">399</div>
          <div className="price">459</div>
        </div>
      </div>
      <div className="offer-group">
        <h2>Smart+ dwustronny</h2>
        <p>
          Dwustronne, aluminiowe kasety wykonane z najwyższej jakości materiałów w komplecie z czarną, wzmocnioną torbą
          transportową. Występuje w 3 rozmiarach 85, 100 lub 120 cm szerokości na 200 cm wysokości. Opcjonalnie można
          zamówić lampkę do oświetlenia kasety.
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={smartDoubleOpen} alt="roll-up smart dwustronny"/>
          </div>
          <div className="offer-image">
            <img src={smartDouble} alt="roll-up smart dwustronny"/>
          </div>
        </div>
        <div className="offer-table">
          <div className="subtitle">Ceny pln netto za 1 sztukę z wydrukiem / rodzaje podłoża do druku</div>
          <div className="product-name">Szerokość w cm</div>
          <div className="price">85</div>
          <div className="price">100</div>
          <div className="price">120</div>
          <div className="product-name">Blockout</div>
          <div className="price">553</div>
          <div className="price">613</div>
          <div className="price">X</div>
          <div className="product-name">Baner powlekany</div>
          <div className="price">553</div>
          <div className="price">613</div>
          <div className="price">659</div>
          <div className="product-name">Baner ferrari</div>
          <div className="price">633</div>
          <div className="price">693</div>
          <div className="price">749</div>
          <div className="product-name">Poliester</div>
          <div className="price">673</div>
          <div className="price">733</div>
          <div className="price">789</div>
        </div>
      </div>
    </div>
  );
}