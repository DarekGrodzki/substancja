import React from 'react';
import orders from "../../../../assets/images/products/zamowienia.jpg";
import ordersSecond from "../../../../assets/images/products/zamowienia-1.jpg";

export default function Orders() {
  return (
    <div className="content-container">
      <div className="offer-group">
        <h2>Zamówienia</h2>
        <h2>Jak składać zamówienia:</h2>
        <p>
          <span>- mailowo: na adres</span>
          <a href="mailto:info@substancja.eu"> info@substancja.eu,</a>
          <span>podając dokładne informacje zamawianego produktu
            (wymiar, ilość, rodzaj materiału itp.), dane kontaktowe oraz dane do faktury. Prosimy także o podanie formy
             płatności oraz sposobu odbioru.
        </span>
          <span>- telefonicznie.</span>
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={orders} alt="zamówienia"/>
          </div>
          <div className="offer-image">
            <img src={ordersSecond} alt="zamówienia"/>
          </div>
        </div>
        <h2>Płatności:</h2>
        <p>
          <span>Przewidziane są 3 rodzaje płatności:</span>
          <span>- płatność gotówką przy odbiorze/za pobraniem</span>
          <span>- przedpłata należności na konto</span>
          <span>- stali klienci mają możliwość otrzymania faktury z odroczonym terminem płatności</span>
          <span>Przy zamówieniach powyżej 3000 zł brutto wymagamy zaliczki w wysokości 30% wartości faktury.</span>
        </p>
        <h2>Numer konta:</h2>
        <p>
          <span>mBank 58 1140 2004 0000 3902 5802 0139</span>
          <span>Substancja</span>
          <span>05-074 Brzeziny 1L</span>
        </p>
        <h2>Odbiory i wysyłki:</h2>
        <p>
          <span>- Odbiór osobisty w siedzibie firmy (Brzeziny koło Warszawy)</span>
          <span>- Przesyłka kurierska, koszt przesyłki wg. aktualnego cennika przewoźnika, zależny od wagi.</span>
          <span>- Paczka pocztowa. Koszt przesyłki wg. cennika Poczty Polskiej.</span>
        </p>
        <h2>Projekty:</h2>
        <p>
          <span>Jeśli nie posiadają Państwo własnego projektu oferujemy jego stworzenie.</span>
          <span>Jeśli mają Państwo własny projekt prosimy o sprawdzenie czy jest zgodny z poniższą specyfikacją.</span>
          <span>Przygotowanie plików produkcyjnych do druku solwentowego:</span>
        </p>
        <p>
          <span>- Pliki należy przygotowywać w skali 1:1</span>
          <span>- Minimalna rozdzielczość zdjęć to 150 DPI</span>
          <span>
            - Prace powinny być zapisywane w następujących formatach: eps(cs2), PDF, TiFF z kompresją LZW,
            cdr(ver.11, czcionki zamienione na krzywe), jpg.
          </span>
          <span>- Materiały należy przygotowywać w trybie kolorystycznym CMYK 32-bity</span>
          <span>
            - Prace przeznaczone do druku banerów lub siatek należy przygotowywać bez spadów. Prace do wyklejania na
            twardych podłożach muszą mieć spady po 5 mm z każdego boku.
          </span>
          <span>
            - Nie należy dodawać drukarskich znaków cięcia i paserów. W przypadku białego tła należy dodać ramkę
            wyznaczającą granicę projektu.
          </span>
        </p>
      </div>
    </div>
  )
}