import React from 'react';
import cards from '../../../../assets/images/products/wizytowki-substancja.jpg';
import cardsSecond from '../../../../assets/images/products/wizytowki.jpg';

export default function Cards() {
  return (
    <div className="content-container">
    <div className="offer-group">
      <h2>Wizytówki</h2>
      <p>
        Drukujemy je przy mniejszych nakładach w technologii cyfrowej lub przy większych offsetowo. Mamy możliwość 
        wykonania ich w wielu opcjach np.:
        <span>- na papierach niepowlekanych,</span>
        <span>- na papierze kredowym,</span>
        <span>- zalaminowane folią matową lub błyszczącą,</span>
        <span>- w formie kart plastikowych,</span>
        <span>- z selektywnym lakierem wypukłym,</span>
        <span>- tłoczone.</span>
      </p>
      <div className="offer-images">
        <div className="offer-image">
          <img src={cards} alt="wizytówki"/>
        </div>
        <div className="offer-image">
          <img src={cardsSecond} alt="wizytówki"/>
        </div>
      </div>
      <p>Poniżej ceny netto najczęściej przez nas wykonywanych wizytówek.</p>
      <div className="offer-table">
        <div className="subtitle">Kreda 350g plus laminat matowy - rozmiar 90 x 50 mm lub 85 x 55 mm</div>
        <div className="product-name">Rodzaj / Ilość sztuk</div>
        <div className="price">250</div>
        <div className="price">500</div>
        <div className="price">1000</div>
        <div className="price">2000</div>
        <div className="product-name">Jednostronne</div>
        <div className="price">70</div>
        <div className="price">100</div>
        <div className="price">150</div>
        <div className="price">225</div>
        <div className="product-name">Dwustronne</div>
        <div className="price">80</div>
        <div className="price">110</div>
        <div className="price">160</div>
        <div className="price">245</div>
      </div>
      <div className="offer-table">
        <div className="subtitle">
          Kreda 350g, laminat matowy i lakier selektywny, wypukły - rozmiar 90 x 50 mm lub 85 x 55 mm
        </div>
        <div className="product-name">Rodzaj / Ilość</div>
        <div className="price">250</div>
        <div className="price">500</div>
        <div className="product-name">Jednostronne</div>
        <div className="price">120</div>
        <div className="price">160</div>
        <div className="product-name">Dwustronne</div>
        <div className="price">140</div>
        <div className="price">195</div>
      </div>
    </div>
    </div>
  )
}