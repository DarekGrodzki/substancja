import React from 'react';
import popup from "../../../../assets/images/products/pop-up.jpg";
import popupSecond from "../../../../assets/images/products/pop-up-1.jpg";

export default function Popups() {
  return (
    <div className="content-container">
      <div className="offer-group">
        <h2>Ścianki reklamowe - POP UP</h2>
        <p>
          <span>Potrzebujesz ściankę reklamową... to dobrze trafiłeś!</span>
          <span>Produkujemy POP UP'y</span>
          <span>łukowe i proste</span>
          <span>w różnych rozmiarach.</span>
        </p>
        <p>Dlaczego nasze ścianki?</p>
        <p>
          Ponieważ drukujemy grafiki z fotograficzną jakością i zabezpieczamy je bardzo dobrym laminatem strukturalnym
          typu stoplight. Dodatkowo używamy nieprześwitujących podkładów polipropylenowych do naklejania folii, a
          w konstrukcje ścianek zaopatrujemy się u solidnych producentów. Efekt jest taki, że nasze produkcje zadowolą
          nawet najbardziej wymagających klientów.
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={popup} alt="ścianka reklamowa Pop-up"/>
          </div>
          <div className="offer-image">
            <img src={popupSecond} alt="ścianka reklamowa Pop-up"/>
          </div>
        </div>
        <p>Chcesz dowiedzieć się więcej lub otrzymać wycenę?</p>
        <p>
          <span>Zadzwoń lub napisz do nas maila,</span>
          <span>z przyjemnością odpowiemy na wszystkie Twoje pytania.</span>
        </p>

      </div>
    </div>
  )
}