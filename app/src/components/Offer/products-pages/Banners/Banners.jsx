import React from 'react';
import banners from '../../../../assets/images/products/banners.jpg';
import bannersSecond from '../../../../assets/images/products/banners-second.jpg';

export default function Banners() {
  return (
    <div className="content-container">
      <div className="offer-group">
        <h2>Banery reklamowe</h2>
        <p>
          Najpopularniejsza forma reklamy zewnętrznej, najczęściej zawieszane na ścianach budynków lub ogrodzeniach.
          Zaletami naszych banerów oprócz rozsądnych cen jest przede wszystkim bardzo wysoka jakość wydruków przez co
          relacja ich jakości do ceny jest na najwyższym możliwym poziomie. W naszej ofercie posiadamy:
        </p>
        <div className="offer-images">
          <div className="offer-image">
          <img src={banners} alt="banery reklamowe"/>
          </div>
          <div className="offer-image">
            <img src={bannersSecond} alt="banery reklamowe"/>
          </div>
        </div>
        <div className="offer-table">
          <div className="subtitle">Ceny pln netto za m2 / dla wielkości całego zamówienia.</div>
          <div className="product-name">Ilość m2</div>
          <div className="price">&lt;3</div>
          <div className="price">3-10</div>
          <div className="price">10-50</div>
          <div className="price">&gt;50</div>
          <div className="product-name">Baner laminowany</div>
          <div className="price">33</div>
          <div className="price">28</div>
          <div className="price">23</div>
          <div className="price">18</div>
          <div className="product-name">Baner powlekany</div>
          <div className="price">43</div>
          <div className="price">38</div>
          <div className="price">33</div>
          <div className="price">28</div>
          <div className="product-name">Baner dwustronny</div>
          <div className="price">70</div>
          <div className="price">65</div>
          <div className="price">60</div>
          <div className="price">55</div>
          <div className="product-name">Baner odblaskowy</div>
          <div className="price">70</div>
          <div className="price">65</div>
          <div className="price">60</div>
          <div className="price">55</div>
          <div className="product-name">Siatka mesh</div>
          <div className="price">38</div>
          <div className="price">33</div>
          <div className="price">28</div>
          <div className="price">23</div>
        </div>
      </div>
    </div>
  )
}