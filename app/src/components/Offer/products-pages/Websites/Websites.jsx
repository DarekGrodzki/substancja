import React from 'react';
import websites from "../../../../assets/images/products/www.jpg";
import websitesSecond from "../../../../assets/images/products/www-1.jpg";

export default function Websites() {
  return (
    <div className="content-container">
      <div className="offer-group">
        <h2>Strony www</h2>
        <p>
          <span>
            - potrzebujesz wydrukować plakaty? Drukujemy już od nakładu 1 szt. Maksymalna szerokość to 150 cm, długość
          dowolna. Wydruki w pełnym kolorze z fotograficzną jakością. Drukujemy na kilku rodzajach papieru:
          </span>
          <span>- Papier satynowany(citylight) 135 g/m2,</span>
          <span>- Papier satynowany 200 g/m2,</span>
          <span>- Papier fotograficzny/błyszczaćy 260 g/m2,</span>
          <span>- Blueback 115 g/m2 lub</span>
          <span>- Papier lateksowy.</span>
        </p>
        <p>
          Jeżeli potrzebujesz dużego nakładu plakatów, chętnie wycenimy je dla Ciebie także w technologii offsetowej.
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={websites} alt="strony www"/>
          </div>
          <div className="offer-image">
            <img src={websitesSecond} alt="strony www"/>
          </div>
        </div>
        <div className="offer-table">
          <div className="subtitle">Ulotki offsetowe, dwustronne, kreda matowa 150g</div>
          <div className="product-name">Format / Ilość</div>
          <div className="price">1000</div>
          <div className="price">2000</div>
          <div className="product-name">DL</div>
          <div className="price">156</div>
          <div className="price">200</div>
          <div className="product-name">A6</div>
          <div className="price">146</div>
          <div className="price">170</div>
          <div className="product-name">A5</div>
          <div className="price">232</div>
          <div className="price">300</div>
          <div className="product-name">A4</div>
          <div className="price">382</div>
          <div className="price">472</div>
        </div>
      </div>
    </div>
  )
}