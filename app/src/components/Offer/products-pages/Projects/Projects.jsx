import React from 'react';
import projects from '../../../../assets/images/products/projekty.jpg';
import projectsSecond from '../../../../assets/images/products/projekty-1.jpg';

export default function Projects() {
  return (
    <div className="content-container">
      <div className="offer-group">
        <h2>Projekty</h2>
        <p>
          Jesteśmy młodym, kreatywnym zespołem z głowami pełnymi pomysłów. Znajdziemy optymalne rozwiązania dla Twojej
          firmy, a nasza wiedza z zakresu marketingu pozwoli na idealne dopasowanie produktów do Twoich potrzeb.
          Projektujemy pojedyncze prace takie jak np. projekt banera, roll-up'u lub wizytówek, a także kompleksowe
          kampanie reklamowe. Na nasze usługi składają się między innymi:
        </p>
        <p>
          <span>* symbole firmy (znak i logotyp firmowy),</span>
          <span>* wygląd strony www,</span>
          <span>* druki firmowe (papier firmowy, koperty i inne),</span>
          <span>* materiały reklamowe,</span>
          <span>* znakowanie środków transportu,</span>
          <span>* tablice i tabliczki informacyjne, szyldy reklamowe,</span>
          <span>* promocja Państwa produktów i usług.</span>
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={projects} alt="projekty"/>
          </div>
          <div className="offer-image">
            <img src={projectsSecond} alt="projekty"/>
          </div>
        </div>
        <p>
          <span>Koszt prac graficznych rozliczamy według stawki:</span>
          <span>60 zł netto za 1h,</span>
          <span>minimalka - 15 zł netto.</span>
        </p>
      </div>
    </div>
  )
}