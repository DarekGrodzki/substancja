import React from 'react';
import stickers from '../../../../assets/images/products/naklejki.jpg';
import stickersSecond from '../../../../assets/images/products/naklejki-1.jpg';

export default function Stickers() {
  return (
    <div className="content-container">
      <div className="offer-group">
        <h2>Naklejki</h2>
        <h2>Folia samoprzylepna z nadrukiem</h2>
        <p>
          - bardzo wszechstronne zastosowanie, przeznaczona do ekspozycji zewnętrznych jak i wewnętrznych.
          Stosuje się ją na płaskie powierzchnie, do wyklejania witryn sklepowych oraz jako zwykłe małe naklejki.
        </p>
        <h2>Folia One Way Vision</h2>
        <p>
          - folia perforowana, samoprzylepna - stosowana do wyklejania szyb samochodowych i witryn. Jej perforowana
          struktura pozwala na stosowanie wszędzie tam gdzie potrzebna jest dobra widoczność od wewnątrz.
        </p>
        <h2>Folia odblaskowa z nadrukiem</h2>
        <p>
          - doskonałe rozwiązanie na tablice reklamowe lub do częściowego oklejania samochodów, sprawia, że reklamy
          są widoczne przez całą dobę np. w świetle latarni lub reflektorów samochodowych.
        </p>
        <h2>Laminacja wydruków</h2>
        <p>- dodatkowe zabezpieczenie nadruku przed warunkami atmosferycznymi oraz uszkodzeniami mechanicznymi,
          ułatwia także aplikację folii.
        </p>
        <h2>Wycinanie naklejek po obrysie</h2>
        <p>
          - oferujemy cięcie naklejek do dowolnego kształtu, zarówno z folii zadrukowanej jak i innych folii kolorowych.
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={stickers} alt="naklejki"/>
          </div>
          <div className="offer-image">
            <img src={stickersSecond} alt="naklejki"/>
          </div>
        </div>
      </div>
    </div>
  )
}