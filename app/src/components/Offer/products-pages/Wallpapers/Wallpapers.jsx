import React from 'react';
import paintings from '../../../../assets/images/products/obrazy.jpg';
import paintingsSecond from '../../../../assets/images/products/obrazy-1.jpg';
import wallpaper from '../../../../assets/images/products/fototapety.jpg';
import wallpaperSecond from '../../../../assets/images/products/fototapety-1.jpg';

export default function Posters() {
  return (
    <div className="content-container">
      <div className="offer-group">
        <h2>Obrazy i fototapety</h2>
        <h2>Obrazy</h2>
        <p>
          <span>
            - potrzebujesz wydrukować plakaty? Drukujemy już od nakładu 1 szt. Maksymalna szerokość to 150 cm, długość
          dowolna. Wydruki w pełnym kolorze z fotograficzną jakością. Drukujemy na kilku rodzajach papieru:
          </span>
          <span>- Papier satynowany(citylight) 135 g/m2,</span>
          <span>- Papier satynowany 200 g/m2,</span>
          <span>- Papier fotograficzny/błyszczaćy 260 g/m2,</span>
          <span>- Blueback 115 g/m2 lub</span>
          <span>- Papier lateksowy.</span>
        </p>
        <p>
          Jeżeli potrzebujesz dużego nakładu plakatów, chętnie wycenimy je dla Ciebie także w technologii offsetowej.
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={paintings} alt="obrazy"/>
          </div>
          <div className="offer-image">
            <img src={paintingsSecond} alt="obrazy"/>
          </div>
        </div>
        <div className="offer-table">
          <div className="subtitle">Ulotki offsetowe, dwustronne, kreda matowa 150g</div>
          <div className="product-name">Format / Ilość</div>
          <div className="price">1000</div>
          <div className="price">2000</div>
          <div className="product-name">DL</div>
          <div className="price">156</div>
          <div className="price">200</div>
          <div className="product-name">A6</div>
          <div className="price">146</div>
          <div className="price">170</div>
          <div className="product-name">A5</div>
          <div className="price">232</div>
          <div className="price">300</div>
          <div className="product-name">A4</div>
          <div className="price">382</div>
          <div className="price">472</div>
        </div>
      </div>
      <div className="offer-group">
        <h2>Fototapety</h2>
        <p>
          - chciałbyś nakleić tapetę na ścianę ze swoim zdjęciem lub z własnym projektem? Nie ma problemu, drukujemy na
          materiale specjalnie do tego celu przeznaczonym. Aplikujesz taki wydruk identycznie jak tapetę czyli na klej
          do tapet.
        </p>
        <div className="offer-images">
          <div className="offer-image">
            <img src={wallpaper} alt="fototapeta"/>
          </div>
          <div className="offer-image">
            <img src={wallpaperSecond} alt="fototapeta"/>
          </div>
        </div>
        <div className="offer-table">
          <div className="subtitle">Ulotki offsetowe, dwustronne, kreda matowa 150g</div>
          <div className="product-name">Format / Ilość</div>
          <div className="price">1000</div>
          <div className="price">2000</div>
          <div className="product-name">DL</div>
          <div className="price">156</div>
          <div className="price">200</div>
          <div className="product-name">A6</div>
          <div className="price">146</div>
          <div className="price">170</div>
          <div className="product-name">A5</div>
          <div className="price">232</div>
          <div className="price">300</div>
          <div className="product-name">A4</div>
          <div className="price">382</div>
          <div className="price">472</div>
        </div>
      </div>
    </div>
  )
}