import React from 'react';
import './OfferNavLink.css';
import { NavLink } from 'react-router-dom';

export default function OfferNavLinks (props) {

  function handleClick() {
    props.scrollToProductsSection();
  }

  return (
    <NavLink to={`/oferta/${props.data[1]}`}
             className="offer-button"
             activeClassName="active-offer-button"
             onClick={handleClick}>
      <span>{props.data[0]}</span>
      <img src={props.data[3]} alt={props.data[0]} />
    </NavLink>
  );
}