import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { animateScroll } from 'react-scroll';
import './Offer.css';
import PageDivider from '../Page-Divider/PageDivider';
import Promotion from './Promotion/Promotion';
import Banners from './products-pages/Banners/Banners';
import Billboards from './products-pages/Billboards/Billboards';
import Cards from './products-pages/Cards/Cards';
import Flyers from './products-pages/Flyers/Flyers';
import Orders from './products-pages/Orders/Orders';
import Popups from './products-pages/Popups/Popups';
import Wallpapers from './products-pages/Wallpapers/Wallpapers';
import Projects from './products-pages/Projects/Projects';
import Rollups from './products-pages/Rollups/Rollups';
import Stickers from './products-pages/Stickers/Stickers';
import Websites from './products-pages/Websites/Websites';
import Wrapping from './products-pages/Wrapping/Wrapping';
import OfferNavLink from './Offer-Nav-Link/OfferNavLink';
import banners from '../../assets/images/offer-icons/banners.png';
import cards from '../../assets/images/offer-icons/cards.png';
import billboards from '../../assets/images/offer-icons/billboards.png';
import flyers from '../../assets/images/offer-icons/flyers.png';
import orders from '../../assets/images/offer-icons/orders.png';
import popups from '../../assets/images/offer-icons/popups.png';
import wallpapers from '../../assets/images/offer-icons/wallpapers.png';
import projects from '../../assets/images/offer-icons/projects.png';
import rollups from '../../assets/images/offer-icons/rollups.png';
import stickers from '../../assets/images/offer-icons/stickers.png';
import websites from '../../assets/images/offer-icons/websites.png';
import wrapping from '../../assets/images/offer-icons/wrapping.png';

class Offer extends Component {

  constructor () {
    super();
    this.productsSection = React.createRef();
  }

  offerPageLinks = [
    ['Roll up\'y', 'rollupy', Rollups, rollups],
    ['Banery reklamowe', 'banery', Banners, banners],
    ['Naklejki', 'naklejki', Stickers, stickers],
    ['Ścianki reklamowe', 'popupy', Popups, popups],
    ['Oklejanie', 'oklejanie', Wrapping, wrapping],
    ['Tablice', 'tablice', Billboards, billboards],
    ['Obrazy i fototapety', 'obrazy_i_fototapety', Wallpapers, wallpapers],
    ['Wizytówki', 'wizytowki', Cards, cards],
    ['Ulotki i plakaty', 'ulotki_i_plakaty', Flyers, flyers],
    ['Projekty', 'projekty', Projects, projects],
    ['Strony www', 'strony_www', Websites, websites],
    ['Jak zamawiać', 'zamowienia', Orders, orders],
  ];

  scrollToProductsSection = () => {
    const position = this.productsSection.current.getBoundingClientRect().bottom + window.pageYOffset -30;
    animateScroll.scrollTo(position, {duration: 700, smooth: true});
  };

  links = this.offerPageLinks.map((element) => {
    return (
      <OfferNavLink data={element} scrollToProductsSection={this.scrollToProductsSection} key={element[0]}/>
    )
  });

  routes = this.offerPageLinks.map((element) => {
    return <Route path={`/oferta/${element[1]}`}
                  component={element[2]}
                  key={element[1]} />
  });

  render () {
    return (
      <section className="offer">
        <div className="offer-navigation-holder">
          <div className="offer-navigation" ref={this.productsSection}>
            {this.links}
          </div>
        </div>
        <PageDivider/>
        <Promotion/>
        <Switch>
          {this.routes}
          <Route />
        </Switch>
      </section>
    );
  }
}

export default Offer;