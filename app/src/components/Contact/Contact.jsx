import React from 'react';
import './Contact.css';
import logo from '../../assets/images/substancja-logo.png'
import tel from '../../assets/images/tel-contact.png'
import mail from '../../assets/images/mail-contact.png'
import MapContainer from './Map/Map';

export default function Contact () {
  return (
    <div className="content-container">
      <div className="contact">
        <img src={logo} alt="logo Substancja" />
        <div className="phone-mail-container">
          <img src={tel} alt="telephone icon" />
          <div>
            <h2>tel.: 022 760 33 16</h2>
            <h2>tel. kom.: 691 75 62 75</h2>
            <h2>
              <a href="mailto:info@substancja.eu">info@substancja.eu</a>
            </h2>
          </div>
          <img src={mail} alt="mail icon" />
        </div>
        <h2>Brzeziny 1L, 05-074 Halinów</h2>
        <h2>
          <a
            href="https://www.google.com/maps/dir/?api=1&destination=Substancja%2CBanery+reklamowe%2CRoll+upy
            %2COklejanie+witryn+i+samochodów&destination_place_id=ChIJb7T59wsqH0cRFkt6kGhyk58"
             target="_blank"
             rel="noopener noreferrer"
          >
            Sprawdź jak dojechać
          </a>
        </h2>
        <p>pn. - pt. 9 - 17
          <span>(prosimy o kontakt przed przyjazdem)</span>
        </p>
        <h2>Mapka dojazdu</h2>
        <MapContainer />
      </div>
    </div>
  )
}