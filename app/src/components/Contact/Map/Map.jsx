import React, { Component } from "react";
import './Map.css';
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";
import substancja from '../../../assets/images/substancja-icon.png';
import { googleApiKey } from "../../../consts/GoogleApiKey";


export class MapContainer extends Component {
  
  constructor (props) {
    super(props);
    this.googleMap = React.createRef();
    this.points = [
      {lat: 52.279799, lng: 21.021617},
      {lat: 52.142004, lng: 21.032811},
      {lat: 52.194088, lng: 21.502234}
    ];
    this.bounds = new this.props.google.maps.LatLngBounds();

    this.state = {
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {}
    };
  }

  componentDidMount () {
    this.points.forEach((point) => {
      this.bounds.extend(point);
    });
    this.googleMap.current.map.fitBounds(this.bounds);
  }

  onMarkerClick = (props, marker) => {
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    });
  };

  render () {

    return (
      <div className="map-wrapper">
        <Map
          ref={this.googleMap}
          google={this.props.google}
          initialCenter={{
            lat: 52.196485,
            lng: 21.374650
          }}
          style={{
            width: '100%',
            height: '100%'
          }}
          bounds={this.bounds}
        >
          <Marker
            onClick={this.onMarkerClick}
            icon={{
              url: substancja
            }}
          />
          <InfoWindow
            marker={this.state.activeMarker}
            visible={this.state.showingInfoWindow}>
            <div>
              <h3>Zapraszamy :)</h3>
            </div>
          </InfoWindow>
        </Map>
      </div>

    );
  }
}

export default GoogleApiWrapper({
  apiKey: googleApiKey
})(MapContainer);