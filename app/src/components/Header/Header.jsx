import React, {Component} from 'react';
import {animateScroll} from 'react-scroll';
import './Header.css';
import PageDivider from '../Page-Divider/PageDivider';
import HeaderNavLinks from './Header-Nav-Link/HeaderNavLink';
import Hamburger from './Hamburger/Hamburger';
import ConceptDiv from './Concept-Div/ConceptDiv';
import ScrollToTop from './Scroll-To-Top/ScrollToTop';
import logo from '../../assets/images/substancja-logo.png';

class Header extends Component {

  constructor() {
    super();

    this.state = {
      fixedMenu: false,
      goToTop: false,
      mobileMenuVisible: false
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.checkScrollPosition);
  }

  linksData = [
    ['/', 'Home'],
    ['/oferta', 'Oferta'],
    ['/portfolio', 'Portfolio'],
    ['/kontakt', 'Kontakt']
  ];

  checkScrollPosition = () => {
    if (window.pageYOffset < 40) {
      this.setState({goToTop: false});
      if (window.innerWidth > 769) {
        this.setState({fixedMenu: false});
      }
    } else {
      this.setState({fixedMenu: true, goToTop: true});
    }
  };

  toggleMobileMenuVisibility = () => {
    this.setState({mobileMenuVisible: !this.state.mobileMenuVisible});
  };

  hideMobileMenu = () => {
    this.setState({mobileMenuVisible: false});
  };

  scrollToTop = () => {
    animateScroll.scrollToTop({duration: 1000, smooth: true});
  };

  navigationLinks = this.linksData.map((element) => {
    return <HeaderNavLinks data={element} hideMobileMenu={this.hideMobileMenu} key={element[0]} />
  });

  render() {
    return (
      <header>
        <div className="header-content">
          <div className={`nav-bar ${this.state.fixedMenu ? "nav-bar-fixed" : ""}`}>
            <div className="content-container">
              <nav>
                <img src={logo} alt="logo Substancja" />
                <ul className={`menu ${!this.state.mobileMenuVisible ? "blank-mobile-menu" : ""}`}>
                  {this.navigationLinks}
                </ul>
                <Hamburger data={this.state.mobileMenuVisible} toggleMobileMenuVisibility={this.toggleMobileMenuVisibility} />
              </nav>
            </div>
          </div>
          <ConceptDiv />
        </div>
        <PageDivider />
       <ScrollToTop data={this.state.goToTop} scrollToTop={this.scrollToTop}/>
      </header>
    );
  }
}

export default Header;