import React from 'react';
import './ScrollToTop.css';
import scrollToTop from "../../../assets/images/scroll-to-top.png";

export default function ScrollToTop (props) {

  function handleClick() {
    props.scrollToTop();
  }

  return (
    <div className={`go-to-top ${props.data ? "" : "blank"}`}
         onClick={handleClick}>
      <img src={scrollToTop} alt="scroll to top" />
    </div>
  );
}