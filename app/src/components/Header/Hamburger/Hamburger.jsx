import React from 'react';
import './Hamburger.css';

export default function Hamburger (props) {

  function handleClick() {
    props.toggleMobileMenuVisibility();
  }

  return (
    <div className={`hamburger ${props.data ? "active-hamburger" : ""}`}
         onClick={handleClick}
    >
      <div className="closed-hamburger">
        <span/>
        <span/>
        <span/>
      </div>
      <div className="opened-hamburger">
        <span/>
        <span/>
      </div>
    </div>
  );
}