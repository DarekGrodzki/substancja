import React from 'react';
import './HeaderNavLink.css';
import { NavLink } from 'react-router-dom';

export default function HeaderNavLinks (props) {

  function handleClick() {
    props.hideMobileMenu();
  }

  return (
    <li className="link-to">
      <NavLink
        exact={props.data[1] === 'Home'}
        to={props.data[0]}
        className="link"
        activeClassName="active-link"
        onClick={handleClick}
      >
        <div className="active-side">{props.data[1]}</div>
        <div className="inactive-side">{props.data[1]}</div>
      </NavLink>
    </li>
  );
}