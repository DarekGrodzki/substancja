import React from 'react';
import './Footer.css';
import fb from '../../assets/images/fb.png';
import tel from '../../assets/images/tel.png';
import mail from '../../assets/images/mail.png';
import logo from '../../assets/images/substancja-logo.png';

export default function Footer() {
  return (
    <footer>
    <div className='content-container'>
      <div className='footer-content'>
        <div>
          <a
            href='https://www.facebook.com/Substancja-317585418285897/'
            target='_blank'
            rel='noopener noreferrer'
          >
            <img src={fb} alt='fb-icon' />
            <p>Nasz Facebook</p>
          </a>
        </div>
        <div>
          <a href='mailto:info@substancja.eu'>
            <img src={mail} alt='mail-icon' />
            <p>info@substancja.eu</p>
          </a>
        </div>
        <div>
          <a href='tel:+48691756275'>
            <img src={tel} alt='tel-icon' />
            <p>691 75 62 75</p>
          </a>
        </div>
        <div>
          <a href='https://www.substancja.eu/'
             target='_self'
          >
            <h2>&copy;</h2>
            <img src={logo} alt='Substancja logo' />
          </a>
        </div>
      </div>
    </div>
  </footer>
  )
}

