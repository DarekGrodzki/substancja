import React from 'react';
import './PageDivider.css';

export default function PageDivider() {

  return (
    <div className='page-divider' />
  );
}