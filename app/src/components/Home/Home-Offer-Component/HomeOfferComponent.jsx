import React, {Component} from 'react';
import './HomeOfferComponent.css';
import {NavLink} from 'react-router-dom';

class HomeOfferComponent extends Component {

  constructor(props) {
    super(props);

    this.state = {
      rotateSection: false
    }
  }

  handleClickRotateSection = () => {
    this.setState({rotateSection: !this.state.rotateSection});
  };

  products = this.props.data.products.map((product) => {
    return (
      <h3 key={product[0]}>
        <NavLink to={`/oferta/${product[1]}`}>
          {product[0]}
        </NavLink>
      </h3>
    )
  });

  render() {
    return (
      <article
        className={`section ${this.state.rotateSection ? "section-rotated" : ""}`}
      >
        <div className="article-front">
          <img src={this.props.data.picture} alt="ikona sekcji" />
          <h2>{this.props.data.title}</h2>
          {this.products}
          <h4 onClick={this.handleClickRotateSection}>Dowiedz się więcej</h4>
        </div>
        <div className="article-back" onClick={this.handleClickRotateSection}>
          <img src={this.props.data.picture} alt="ikona sekcji" />
          <h2>{this.props.data.title}</h2>
          <p>{this.props.data.description}</p>
          <h4 onClick={this.handleClickRotateSection}>Wróć</h4>
        </div>
      </article>
    );
  }
}

export default HomeOfferComponent;