import React from 'react';
import LazyLoad from 'react-lazy-load';
import './Home.css';
import HomeOfferComponent from './Home-Offer-Component/HomeOfferComponent';
import largeFormat from '../../assets/images/large-format.png';
import smallFormat from '../../assets/images/small-format.png';
import agency from '../../assets/images/agency.png';
import logo from '../../assets/images/substancja-logo.png';

export default function Home () {

  const categoryData = {
    wideFormatPrinting: {
      title: 'Druk wielkoformatowy',
      picture: largeFormat,
      products: [
        ['Roll up\'y', 'rollupy'],
        ['Ścianki reklamowe', 'popupy'],
        ['Bannery reklamowe', 'banery'],
        ['Plakaty', 'plakaty'],
        ['Naklejki', 'naklejki'],
        ['Oklejanie witryn', 'oklejanie'],
        ['Oklejanie samochodów', 'oklejanie']
      ],
      description: 'Nasza drukarnia wielkoformatowa specjalizuje się w różnego rodzaju stojakach reklamowych typu ' +
      'roll up, ścankach POP-UP oraz innych produktach gdzie ważna jest przede wszystkim jakość druku. Oczywiście ' +
      'produkujemy także banery reklamowe, naklejki oraz plakaty. Drukujemy na sprzęcie marki Roland o maksymalnej ' +
      'rozdzielczości 1440 dpi. Zawsze korzystamy z wysokich rozdzielczości i odpowiednich profili kolorystycznych, ' +
      'dzięki czemu uzyskujemy fotograficzną jakość wydruków oraz eliminujemy wszelakie wady, jak choćby paskowanie ' +
      'itp..'
    },
    smallFormatPrinting: {
      title: 'Druk w małym formacie',
      picture: smallFormat,
      products: [
        ['Roll up\'y', 'rollupy'],
        ['Ścianki reklamowe', 'popupy'],
        ['Bannery reklamowe', 'banery'],
        ['Plakaty', 'plakaty'],
        ['Naklejki', 'naklejki'],
        ['Oklejanie witryn', 'oklejanie'],
        ['Oklejanie samochodów', 'oklejanie']
      ],
      description: 'W zależności od nakładów, terminów realizacji oraz możliwości technologicznych wykorzystujemy ' +
      'druk cyfrowy lub offsetowy. Drukujemy szeroką paletę produktów takich jak: ulotki, wizytówki, foldery, ' +
      'kalendarze itp. Posiadamy również profesjonalny sprzęt introligatorski pozwalający na estetyczne wykańczanie ' +
      'prac.'
    },
    advertisingAgency: {
      title: 'Studio reklamy',
      picture: agency,
      products: [
        ['Roll up\'y', 'rollupy'],
        ['Ścianki reklamowe', 'popupy'],
        ['Bannery reklamowe', 'banery'],
        ['Plakaty', 'plakaty'],
        ['Naklejki', 'naklejki'],
        ['Oklejanie witryn', 'oklejanie'],
        ['Oklejanie samochodów', 'oklejanie']
      ],
      description: 'Do wszystkich naszych produktów oferujemy możliwość stworzenia indywidualnego projektu. ' +
      'Więcej szczegółów w zakładce Projekty. Działamy także jak klasyczna agencja reklamowa więc nie ograniczamy ' +
      'się tylko do produktów które sami bezpośrednio produkujemy. Możesz nam zlecić stworzenie i przeprowadzenie ' +
      'całej kampanii reklamowej lub po prostu oszczędzić czas i zamówić cały wachlarz produktów związanych z ' +
      'reklamą w jednym miejscu a nie zlecać je w wielu różnych firmach.'
    }
  };

  return (
    <div className="content-container">
      <div className="home-content">
        <div className="main-text-holder">
          <h1>Witamy na stronie firmy Substancja.</h1>
          <h2>
            Połączyliśmy potencjał dwóch substancji: agencji reklamowej i drukarni. Artystyczny świat kreacji może
            więc korzystać z całej palety precyzyjnych technologii wyrażania myśli i koncepcji. Co to oznacza dla
            naszych klientów? Przede wszystkim niższe koszty produkcji reklam (typu: stojaki roll up, banery reklamowe,
            naklejki, plakaty itd.) oraz dużą oszczędność czasu. Łatwiej jest także wyeliminować wszelkie problemy,
            występujące często na linii agencja reklamowa - drukarnia.
          </h2>
        </div>
        <div className="articles-holder">
          <HomeOfferComponent data={categoryData.wideFormatPrinting} />
          <HomeOfferComponent data={categoryData.smallFormatPrinting} />
          <HomeOfferComponent data={categoryData.advertisingAgency} />
        </div>
        <div className="main-text-holder home-more">
          <h2>Poniżej przedstawiamy kilka ciekawych wydarzeń z życia naszej firmy:</h2>
          <h3>16.08.2016</h3>
          <h4>Konkurs z nagrodami</h4>
          <p>
            Zorganizowaliśmy konkurs, w którym do wygrania były drukowane przez nas obrazy. To jestmoment losowania
            zwycięskich szczęśliwców.
          </p>
          <div className="i-frame-wrapper">
            <LazyLoad>
              <iframe
                title="konkurs Substancja"
                src="https://www.youtube.com/embed/FtNKtupdxN8?rel=0"
                frameBorder="0"
                allowFullScreen
              />
            </LazyLoad>
          </div>
          <hr />
          <h3>15.03.2016</h3>
          <h4>A oto filmik z oklejonego przez nas ośrodka szoleniowego Akademi Altkom.</h4>
          <p>
            Udekorowaliśmy go m.in. w wiele ciekawych grafik ściennych, wyciętych z kolorowej folii oraz obrazów
            nadrukowanych na płytach PVC. A oto efekt.
          </p>
          <div className="i-frame-wrapper">
            <LazyLoad>
              <iframe
                title="Altkom Akademia"
                src="https://www.youtube.com/embed/uoP-ro_PReY?rel=0"
                frameBorder="0"
                allowFullScreen
              />
            </LazyLoad>
          </div>
          <hr />
          <h3>21.01.2015</h3>
          <h4>Zmieniamy nasze logo :)</h4>
          <p>W zwiąku z tym, że pragniemy ciągle się rozwijać i udoskonalać działalność naszej firmy,
            postanowiliśmy trochę odświeżyć nasz logotyp. Teraz będzie bardziej kolorowy i mamy nadzieję,
            że będzie się Wam bardziej podobał.
          </p>
          <h4>A oto nasze nowe logo:</h4>
          <img src={logo} alt="logo Substancja" />
          <hr/>
          <h3>29.11.2013</h3>
          <h4>Substancja na czarnym lądzie!</h4>
          <p>
            Naszym ostatnim osiągnięciem jest nawiązanie współpracy z Fundacją Kasisi założoną przez Szymona Hołownię.
            Jest to Fundacja, która pomaga opiekować się domem dziecka w Zambii. Jednym z ich ostatnich projektów jest
            zakup ambulansu dla sierocińca. Dzięki niemu chore dzieci mogą szybko dotrzeć do szpitala po wyboistych
            zambijskich drogach. My dołożyliśmy swoją cegiełkę do tego szlachetnego przedsięwzięcia. Zaprojektowaliśmy
            i wydrukowaliśmy naklejki na ambulans, dzięki czemu jest on dobrze oznakowany i widoczny. Teraz nawet lwy
            przed nim uciekają :) Staramy się też pomagać przy realizacji kolejnych projektów, do czego i Was zachęcamy!
          </p>
          <p>
            Więcej szczegółów na stronie&nbsp;
            <a
              href="http://www.fundacjakasisi.pl/"
              rel="nofollow noopener noreferrer"
              target="_blank"
            >
              www.fundacjakasisi.pl
            </a>
          </p>
          <div className="i-frame-wrapper">
            <LazyLoad>
              <iframe
                title="fundacja Kasisi"
                src="https://www.youtube.com/embed/_avrHRefwi4"
                frameBorder="0"
                allowFullScreen
              />
            </LazyLoad>
          </div>
          <hr />
        </div>
      </div>
    </div>
  )
}
