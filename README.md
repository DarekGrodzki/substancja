# Substancja

This project was created with React-Crete-App. Link to production version - [www.substancja.eu](https://www.substancja.eu)


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
